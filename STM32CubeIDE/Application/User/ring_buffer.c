/* ********************************************************************
 * @file ring_buffer.c
 * @author sebasms1.99@gmail.com
 * @brief  Ring buffer variables and APIs.
 * @version V.1.1
 * @date 06-01-2023
 * ********************************************************************
 */

/* Includes ---------------------------------------------------------*/
#include "ring_buffer.h"
#include <stdbool.h>

/* Private Defines --------------------------------------------------*/
/* Private typedef --------------------------------------------------*/
/* Private macro ----------------------------------------------------*/
/* Private variables declaration ------------------------------------*/
/* Private function prototypes --------------------------------------*/
/* Functions definition ---------------------------------------------*/


/**
 * @brief This function initializes a ring buffer strucure based on some given parameters.
 * @param ring_buffer: pointer to the structure to be initialized.
 * @param p_buffer: physical memory address to save the data.
 * @param len: length of the memory allocated for the buffer.
 * @retval 0: If initialization failed.
 * @retval id: otherwise.
 */
uint8_t ring_buffer_init(ring_buffer_t *ring_buffer, uint8_t *p_buffer, size_t len)
{
    //assert(ring_buffer);
    if ((p_buffer == NULL) || (len == 0))
    {
        return 0;
    }

    ring_buffer->buffer = p_buffer;
    ring_buffer->max    = len;

    ring_buffer_reset(ring_buffer);

    return 1;
}

/**
 * @brief This function resets the structure's management variables.
 * @param ring_buffer: pointer to the structure to be reset.
 * @retval None.
 */
void ring_buffer_reset(ring_buffer_t *ring_buffer)
{
    ring_buffer->head = 0;
    ring_buffer->tail = 0;
    ring_buffer->full = 0;
}

/**
 * @brief To check if the rind buffer is full.
 * @param ring_buffer: pointer to the structure to query.
 * @retval 0: Not full
 * @retval 1: Full
 */
uint8_t ring_buffer_full(volatile ring_buffer_t *ring_buffer)
{
    return ring_buffer->full;
}

/**
 * @brief To check if ring buffer is empty.
 * @param ring_buffer: pointer to the structure to query.
 * @retval 0: Not empty
 * @retval 1: Empty
 */
uint8_t ring_buffer_empty(volatile ring_buffer_t *ring_buffer)
{
    return ((ring_buffer->full == 0) && (ring_buffer->head == ring_buffer->tail));
}

/**
 * @brief To check ring buffer max capacity
 * @param ring_buffer: pointer to the structure to query.
 * @retval Value of the buffer's length give in the initializations.
 */
size_t ring_buffer_capacity(volatile ring_buffer_t *ring_buffer)
{
    return ring_buffer->max;
}

/**
 * @brief This function extracts the length used in the ring buffer or the number of elements we have in the ring buffer.
 * @param ring_buffer pointer to the structure to query
 * @retval ring buffer size.
 */
size_t ring_buffer_size(volatile ring_buffer_t *ring_buffer)
{
    size_t size;

    if (ring_buffer_full(ring_buffer))
    {
        return (ring_buffer->max);
    }
    if (ring_buffer->head > ring_buffer->tail)
    {
        size = ring_buffer->head - ring_buffer->tail;
    }else{
        size = ring_buffer->head + ring_buffer->max - ring_buffer->tail;
    }

    return size;
}

/**
 * @brief This function inserts an element of the circular buffer.
 * @param ring_buffer: pointer to the structure to query.
 * @param t_data: input data.
 */
void ring_buffer_put(volatile ring_buffer_t *ring_buffer, uint8_t t_data)
{
    ring_buffer->buffer[ring_buffer->head] = t_data;
    ring_buffer->head = (ring_buffer->head + 1) % ring_buffer->max;
    if (ring_buffer->full)
    {
        ring_buffer->tail = (ring_buffer->tail + 1) % ring_buffer->max;
    }
    if (ring_buffer->tail == ring_buffer->head)
    {
        ring_buffer->full = 1;
    }
}

/**
 * @brief This function extracts an element from the ring buffer.
 * @param ring_buffer: pointer to the structure to query.
 * @param p_data: return pointer.
 */
uint8_t ring_buffer_get(volatile ring_buffer_t *ring_buffer, uint8_t *p_data)
{
    if(!ring_buffer_empty(ring_buffer))
    {
        *p_data = ring_buffer->buffer[ring_buffer->tail];
        ring_buffer->tail = (ring_buffer->tail + 1) % ring_buffer->max;
        ring_buffer->full = 0;

        return (1);
    }
    return (0);
}

/**
 * @brief This function returns the pointer to the tail of the ring buffer.
 * @param ring_buffer: pointer to the desired structure where to query the data.
 * @retval None
 */
uint8_t ring_buffer_fetch(volatile ring_buffer_t *ring_buffer)
{
    return &ring_buffer->buffer[ring_buffer->tail];
}

/*-------------------------------------------------------- END OF FILE -------------------------------------------------------------*/
