/**
 * ********************************************************************
 * @file uart_driver.c
 * @author sebasms1.99@gmail.com
 * @brief  UART Parser.
 * @version V.1.1
 * @date 06-01-2023
 * ********************************************************************
 */

/* Includes ---------------------------------------------------------*/
#include "command_parser.h"
#include "stm32f4xx_hal.h"
#include "uart_driver.h"

#include <string.h>

#define FW_VER "V1.0.20230601"

#define RECEIVED_PACKET_MAX_LEN    (128)

#define RX_PACKET_MAX_TIMEOUT_MS   (50)

uart_driver_t uart_driver;
/* Private Defines --------------------------------------------------*/
/* Private typedef --------------------------------------------------*/
/* Private macro ----------------------------------------------------*/
/* Private variables declaration ------------------------------------*/
/* Private function prototypes --------------------------------------*/
/* Functions definition ---------------------------------------------*/

/**
 * @struct rx_packet_state_
 * @brief RX structure definition
 */
typedef enum rx_packet_state_ {
    RX_PACKET_PREAMBLE = 0x00,
    RX_PACKET_DATA     ,

}rx_packet_state_t;


/**
 * @struct received_packed
 * @brief rx structure definition
 */
struct {
	uint8_t buffer[RECEIVED_PACKET_MAX_LEN];
    size_t size;

    rx_packet_state_t state;

 } received_packed;


/**
 * @brief Function get len rx  uart
 * @param uart_driver
 * @return uint8_t
 */
uint8_t uart_driver_get_rx_len(uart_driver_t *uart_driver)
{
	return (ring_buffer_size(&uart_driver->rx_buffer));
}


/**
 * @brief Function get rx  uart
 * @param uart_driver
 * @param p_data
 * @return uint8_t
 */
uint8_t uart_driver_get_rx_data(uart_driver_t *uart_driver, uint8_t *p_data)
{
	return (ring_buffer_get(&uart_driver->rx_buffer, p_data));
}


/**
 * @brief Function store rx  uart
 * @param uart_driver
 * @param data
 */
void uart_driver_store_rx_data(uart_driver_t *uart_driver, uint8_t data)
{
	ring_buffer_put(&uart_driver->rx_buffer, data);
}


/**
 * @brief Function send uart
 * @param uart_driver
 * @param data
 * @param len
 */
void uart_driver_send(uart_driver_t *uart_driver, uint8_t *data, size_t len)
{
	 // Append data to tx buffer
	for (size_t byte = 0; byte < len; byte++) {
		ring_buffer_put (&uart_driver->tx_buffer, data[byte]);
	}

	if (!ring_buffer_empty(&uart_driver->tx_buffer)) {
		 ring_buffer_get(&uart_driver->tx_buffer, &uart_driver->tx_byte);
		 HAL_UART_Transmit_IT(uart_driver->huart, &uart_driver->tx_byte, 1);
	}

}


/**
 * @brief Function init  uart
 * @param uart_driver
 * @param huart
 */
void uart_driver_init(uart_driver_t *uart_driver, UART_HandleTypeDef *huart)
{
    memset(uart_driver, 0, sizeof(uart_driver_t));

    uart_driver->huart = huart;

    ring_buffer_init(&uart_driver->rx_buffer, uart_driver->rx, UART_RX_BUFF_LEN);
    ring_buffer_init(&uart_driver->tx_buffer, uart_driver->tx, UART_TX_BUFF_LEN);

    HAL_UART_Receive_IT(uart_driver->huart, &uart_driver->rx_byte, 1);
 }

 /**
  * @brief Function run  uart
  * @param uart_driver
  */

void uart_driver_run(uart_driver_t *uart_driver)
{
	size_t rx_buff_len = ring_buffer_size(&uart_driver->rx_buffer);
	uint8_t data = 0;

	if (received_packed.state == RX_PACKET_DATA) {
		if ((HAL_GetTick() - uart_driver->rx_tick) > RX_PACKET_MAX_TIMEOUT_MS) {
			 memset(received_packed.buffer, 0, RECEIVED_PACKET_MAX_LEN);
			 received_packed.size = 0;
			 received_packed.state = RX_PACKET_PREAMBLE;
		}
	}

	while (rx_buff_len > 0) {
		switch (received_packed.state){
		    case RX_PACKET_PREAMBLE:
		    	ring_buffer_get(&uart_driver->rx_buffer, &data);
		    	if (data == PROTOCOL_PREAMBLE) {
		    		received_packed.state = RX_PACKET_DATA;
		    	}
		    	break;

		    case RX_PACKET_DATA:
		    	ring_buffer_get(&uart_driver->rx_buffer, &data);

		    	if (data == PROTOCOL_POSAMBLE) {
		    		received_packed.state = RX_PACKET_PREAMBLE;
		    		parse_command(received_packed.buffer);

		    		memset(received_packed.buffer, 0, RECEIVED_PACKET_MAX_LEN);
		    		received_packed.size = 0;
		    	}else {
		    		received_packed.buffer[received_packed.size++] = data;
		    	}
		    	break;

		    default:
		    	break;
		}

		rx_buff_len--;

	}

}


/**
 * @brief Function Callback tx uart
 * @param huart
 */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart->Instance == USART1) {
        if (!ring_buffer_empty(&uart_driver.tx_buffer)) {
        	ring_buffer_get(&uart_driver.tx_buffer, &uart_driver.tx_byte);
        	HAL_UART_Transmit_IT(uart_driver.huart, &uart_driver.tx_byte, 1);
        }
	}
}

/**
 * @brief Function Callback rx  uart
 * @param huart
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart->Instance == USART1) {
		 uart_driver_store_rx_data(&uart_driver, uart_driver.rx_byte);
		 HAL_UART_Receive_IT(uart_driver.huart, &uart_driver.rx_byte, 1);

		 uart_driver.rx_tick = HAL_GetTick();
	}
}
