/*
 * command_parser.c
 *
 *  Created on: 10/06/2023
 *      Author: sebas
 */
#include "command_parser.h"

#include "uart_driver.h"
#include "main.h"

#include <string.h>

extern uart_driver_t uart_driver;
int band_temperature = 0;
int band_readdutycycle = 0;
int band_readtimeunit = 0;
int state_setdutycycle = 0;
int state_door;
int state_heater;
int get_all = 0;

const uint8_t ack_message[] = "*ACK#";
const uint8_t nack_message[] = "*NACK#";
const uint8_t DOPEN_message[] = "*D1#";
const uint8_t DCLOSE_message[] = "*D0#";
const uint8_t V_message[] = "*V1.0.20230601#";
const uint8_t HHIGH_message[] = "*H1#";
const uint8_t HLOW_message[] = "*H0#";
const uint8_t buffertemparser[] = "*T25.10#";

const uint8_t F_message[] = "*F";

uint8_t process_set_command(rx_packet_t *rx_packet)
{
	uint8_t ret_val = 1;
	switch (rx_packet->element){
	    case ELEMENT_DOOR:
	    	if (rx_packet->value == '0'){
	    	   	HAL_GPIO_WritePin(DOOR_GPIO_Port, DOOR_Pin, GPIO_PIN_RESET);
	    	 }else if (rx_packet->value == '1') {
	             HAL_GPIO_WritePin(DOOR_GPIO_Port, DOOR_Pin, GPIO_PIN_SET);
	    	   }
	        break;
	    case ELEMENT_DUTYCYCLE:
	    	if (rx_packet->value == '0'){
	    		state_setdutycycle = 0; // Duty cycle 0%
	    	 }
	    	else if (rx_packet->value == '1') {
	    		state_setdutycycle = 1; // Duty cycle 25%
	    		 }
	    	else if (rx_packet->value == '2') {
	    		state_setdutycycle = 2; // Duty cycle 50%
	    		 }
	    	else if (rx_packet->value == '3') {
	    		state_setdutycycle = 3;  // Duty cycle 75%
	    		 }
	    	else if (rx_packet->value == '3') {
	    		state_setdutycycle = 4;  // Duty cycle 100%
	    		 }else {
	    		state_setdutycycle = 4; // Duty cycle 100%
	    		 }
	    	break;

        default:
           ret_val = 0;
           break;
	}
	return (ret_val);

}

void parse_command(uint8_t *data)
{
	rx_packet_t *packet = (rx_packet_t *) data;
	if (packet->operation == OPERATION_GET) {
		switch (packet->element){
		   case ELEMENT_FW:
			   uart_driver_send(&uart_driver, (uint8_t *)V_message, sizeof(V_message) - 1);
			   break;
		   case  ELEMENT_HEATER:
			   if (HAL_GPIO_ReadPin(HEATER_GPIO_Port, HEATER_Pin) == GPIO_PIN_SET) {
				   uart_driver_send(&uart_driver, (uint8_t *)HHIGH_message, sizeof(HHIGH_message) - 1);
			    } else {
			    	uart_driver_send(&uart_driver, (uint8_t *)HLOW_message, sizeof(HLOW_message) - 1);
		          }
		    	break;
		   case ELEMENT_DOOR:
			   if (HAL_GPIO_ReadPin(DOOR_GPIO_Port, DOOR_Pin) == GPIO_PIN_SET) {
				   uart_driver_send(&uart_driver, (uint8_t *)DOPEN_message, sizeof(DOPEN_message) - 1);
			   } else {
				   uart_driver_send(&uart_driver, (uint8_t *)DCLOSE_message, sizeof(DCLOSE_message) - 1);
			     }
		     	break;
		   case ELEMENT_TEMPERATURE:
			   band_temperature = 1;
			   break;
		   case ELEMENT_DUTYCYCLE:
		       band_readdutycycle = 1;
		       break;
		   case ELEMENT_UNIT:
			   band_readtimeunit = 1;
			   break;
		   case ELEMENT_LED://Get all

			   if (HAL_GPIO_ReadPin(DOOR_GPIO_Port, DOOR_Pin) == GPIO_PIN_SET) {
				   state_door = 1;
			   } else if (HAL_GPIO_ReadPin(DOOR_GPIO_Port, DOOR_Pin) == GPIO_PIN_RESET) {
				   state_door = 0;
			   }
			   if (HAL_GPIO_ReadPin(HEATER_GPIO_Port, HEATER_Pin) == GPIO_PIN_SET) {
				   state_heater = 1;
			  	} else if(HAL_GPIO_ReadPin(HEATER_GPIO_Port, HEATER_Pin) == GPIO_PIN_RESET) {
			  		state_heater = 0;
			  	}
			   get_all = 1;

			   break;

		}

	}else if (packet->operation == OPERATION_SET) {
       if (!process_set_command(packet)) {
    	   uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message) - 1);
       }else{
           uart_driver_send(&uart_driver, (uint8_t *)ack_message, sizeof(ack_message) - 1);
       }
	}else {
        uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message) - 1);
	}

}

