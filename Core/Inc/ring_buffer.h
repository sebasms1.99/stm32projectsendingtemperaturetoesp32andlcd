/**
 * *******************************************************************************
 * @file ring_buffer.h
 * @author sebasms1.99@gmail.com
 * @brief  Ring buffer variables and APIs.
 * @version V.1.1
 * @date 06-01-2023
 * *******************************************************************************
*/

/*Define to prevent recursive include inclusion ------------------------------------------------------------------------------------*/
#ifndef __RING_BUFFER_H
#define __RING_BUFFER_H

/** Includes -----------------------------------------------------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>

/* Exported defines ----------------------------------------------------------------------------------------------------------------*/
/* Exported types  -----------------------------------------------------------------------------------------------------------------*/

/**
 * @struct ring_buffer_
 * @brief Ring buffer structure definition
 */
typedef struct ring_buffer_ {
    uint8_t *buffer;  /*!< The pointer to the physical space in memory for the data*/
    size_t head;      /*!< The pointer to the last entered data */
    size_t tail;      /*!< The pointer to the last requested data */
    size_t max;       /*!< Maximum capacity of the buffer. Used to avoid memory access errors */
    uint8_t full;     /*!< Flag to record when the buffer gets full */

}ring_buffer_t;

/* Exported constants --------------------------------------------------------------------------------------------------------------*/
/* Exported macro  -----------------------------------------------------------------------------------------------------------------*/
/* Exported functions  -------------------------------------------------------------------------------------------------------------*/

uint8_t ring_buffer_init(ring_buffer_t *, uint8_t *, size_t);
void ring_buffer_reset(ring_buffer_t *);
uint8_t ring_buffer_full(volatile ring_buffer_t *);
uint8_t ring_buffer_empty(volatile ring_buffer_t *);
size_t ring_buffer_capacity(volatile ring_buffer_t *);
size_t ring_buffer_size(volatile ring_buffer_t *);
void ring_buffer_put(volatile ring_buffer_t *, uint8_t);
uint8_t ring_buffer_get(volatile ring_buffer_t *, uint8_t *);
uint8_t ring_buffer_fetch(volatile ring_buffer_t *);

#endif /* __RING_BUFFER_H */
/*-------------------------------------------------------- END OF FILE -------------------------------------------------------------*/
