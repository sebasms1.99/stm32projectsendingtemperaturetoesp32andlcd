#ifndef __COMMAND_PARSER_H_INC_
#define __COMMAND_PARSER_H_INC_

#include <stdio.h>
#include <stdlib.h>

#include "protocol.h"

void parse_command(uint8_t *);
extern int band_temperature;
extern int band_readdutycycle;
extern int band_readtimeunit;
extern int state_setdutycycle;
extern int get_all;
extern int state_door;
extern int state_heater;

#endif /* __COMMAND_PARSER_INC_ */
