#include <gui/screen1_screen/Screen1View.hpp>
#include "stm32f4xx_hal.h"
Screen1View::Screen1View()
{
	toggle_image = 0;

	image_passed.setVisible(false);
	image_passed.invalidate();
}

void Screen1View::touchable_image(touchgfx::ScalableImage *image, uint8_t touchacle){
	if(touchacle == 1){
		image->setTouchable(true);
		image->invalidate();
	}else if(touchacle == 0){
		image->setTouchable(false);
		image->invalidate();
	}
}

void Screen1View::setupScreen()
{
    Screen1ViewBase::setupScreen();
}

void Screen1View::tearDownScreen()
{
    Screen1ViewBase::tearDownScreen();
}

void Screen1View::updateTemp(uint8_t new_temp){
	tempArea1.resizeToCurrentText();
	tempArea1.invalidate();
	Unicode::snprintf(tempArea1Buffer, TEMPAREA1_SIZE, "%d", new_temp);
	tempArea1.resizeToCurrentText();
	tempArea1.invalidate();
}

void Screen1View::select_image(){
	if(toggle_image == 0){
		image_passed.setVisible(true);
		image_passed.invalidate();
		image_failed.setVisible(false);
		image_failed.invalidate();
		HAL_GPIO_WritePin(GPIOG, GPIO_PIN_13, GPIO_PIN_SET);
		toggle_image=1;
	}else if(toggle_image == 1){
		image_passed.setVisible(false);
		image_passed.invalidate();
		image_failed.setVisible(true);
		image_failed.invalidate();
		HAL_GPIO_WritePin(GPIOG, GPIO_PIN_13, GPIO_PIN_RESET);
		toggle_image=0;
	}
}
